package com.lucas.fastyammy.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.lucas.fastyammy.DAO.ConfiguracaoFirebase;
import com.lucas.fastyammy.Entidades.Usuario;
import com.lucas.fastyammy.Helper.Base64Custom;
import com.lucas.fastyammy.Helper.Preferencias;
import com.lucas.fastyammy.R;

public class CadastroActivity extends AppCompatActivity {

    private EditText emailEdt;
    private EditText nomeEdt;
    private EditText sobrenomeEdt;
    private EditText senhaEdt;
    private EditText confirmarSenhaEdt;
    private EditText aniversarioEdt;
    private RadioButton semRb;
    private RadioButton veganoRb;
    private RadioButton vegetarianoRb;
    private Button cadastrarBtn;
    private Usuario usuario;
    private FirebaseAuth autenticacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        emailEdt = (EditText) findViewById(R.id.emailCadastroEdt);
        nomeEdt = (EditText) findViewById(R.id.nomeCadastroEdt);
        sobrenomeEdt = (EditText) findViewById(R.id.sobrenomeCadastroEdt);
        senhaEdt = (EditText) findViewById(R.id.senhaCadastroEdt);
        confirmarSenhaEdt = (EditText) findViewById(R.id.senhaConfirmarCadastroEdt);
        aniversarioEdt = (EditText) findViewById(R.id.aniversarioCadastroEdt);
        semRb = (RadioButton) findViewById(R.id.semCadastro);
        veganoRb = (RadioButton) findViewById(R.id.veganoCadastro);
        vegetarianoRb = (RadioButton) findViewById(R.id.vegetarianoCadastro);
        cadastrarBtn = (Button) findViewById(R.id.btnCadastrarCadastro);

        cadastrarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(senhaEdt.getText().toString().equals(confirmarSenhaEdt.getText().toString())){

                    usuario = new Usuario();
                    usuario.setNome(nomeEdt.getText().toString());
                    usuario.setSobrenome(sobrenomeEdt.getText().toString());
                    usuario.setSenha(senhaEdt.getText().toString());
                    usuario.setEmail(emailEdt.getText().toString());
                    usuario.setAniversario(aniversarioEdt.getText().toString());

                    if(semRb.isChecked()){
                        usuario.setRegime("1");
                    }else if(vegetarianoRb.isChecked()){
                        usuario.setRegime("2");
                    }else{
                        usuario.setRegime("3");
                    }

                    cadastrarUsuario();

                }else{
                    Toast.makeText(CadastroActivity.this, "As senhas não correspondem", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void cadastrarUsuario(){
        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        autenticacao.createUserWithEmailAndPassword(
                usuario.getEmail(),
                usuario.getSenha()
        ).addOnCompleteListener(CadastroActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(CadastroActivity.this, "Usuario cadastrado com sucesso", Toast.LENGTH_SHORT).show();
                    String identificadorUsuario = Base64Custom.codificarBase64(usuario.getEmail());
                    FirebaseUser user = task.getResult().getUser();
                    usuario.setId(identificadorUsuario);
                    usuario.salvar();

                    Preferencias preferencias = new Preferencias(CadastroActivity.this);
                    preferencias.salvarUsuario(identificadorUsuario, usuario.getNome());

                    abrirLoginUsuario();

                }else{
                    String erro = "";
                    try{
                        throw task.getException();
                    }catch (FirebaseAuthWeakPasswordException e){
                        erro = "Digite uma senha mais forte";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        erro = "O E-mail digitado é invalido";
                    }catch (FirebaseAuthUserCollisionException e){
                        erro = "Este E-Mail ja está sendo ultilizado";
                    }catch (Exception e){
                        erro = "Erro ao efetuar cadastro";
                        e.printStackTrace();
                    }

                    Toast.makeText(CadastroActivity.this, "Erro: " + erro +".", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void abrirLoginUsuario(){
        Intent intent = new Intent(CadastroActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}

package com.lucas.fastyammy.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lucas.fastyammy.R;

public class HomeActivityWOLogin extends AppCompatActivity {

    private Button btsAbrirActivitybusca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_wologin);

        btsAbrirActivitybusca = (Button) findViewById(R.id.procurarReceita);
        btsAbrirActivitybusca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAbrirTelaBusca = new Intent(HomeActivityWOLogin.this, BuscaActivity.class);
                startActivity(intentAbrirTelaBusca);
            }
        });
    }


}
